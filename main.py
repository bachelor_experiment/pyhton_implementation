from FileHandler import FileHandler
from SortingAlgorithms import Insertionsort
from SortingAlgorithms import Selectionsort
from SortingAlgorithms import Mergesort
from SortingAlgorithms import Quicksort
from time import process_time

fileHandler = FileHandler()
mergesort = Mergesort.Mergesort()
insertionsort = Insertionsort.Insertionsort()
selectionsort = Selectionsort.Selectionsort()
quickSort = Quicksort.Quicksort()

# CHANGE BELOW HERE #
# Change input testdata.
unsortedArray = fileHandler.readfilechar("testChar")

startTime = process_time()
# Change sorting algorithm
quickSort.quicksort(unsortedArray, 0, len(unsortedArray)-1)
endTime = process_time()

# CHANGE ABOVE THIS COMMENT #

print("Sorting operation took: ")
print(endTime - startTime)
