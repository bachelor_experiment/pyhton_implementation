class FileHandler:
    def __init__(self):
        self.pathToTestFolder = "/home/casperbjork/Development/Bachelor_Thesis/testData/"

    def readfileint(self, fileName):
        print("Filename imported from: " + fileName)
        file = open(self.pathToTestFolder + fileName, "r")
        file.readline()
        integerList = []
        for line in file:
                integerList.append(int(line))
        file.close()
        return integerList

    def readfilefloat(self, fileName):
        print("Filename imported from: " + fileName)
        file = open(self.pathToTestFolder + fileName, "r")
        file.readline()
        floatList = []
        for line in file:
            floatList.append(float(line))
        file.close()
        return floatList

    def readfilechar(self, fileName):
        print("Filename imported from: " + fileName)
        file = open(self.pathToTestFolder + fileName, "r")
        file.readline()
        charList = []
        for line in file:
            line = line.replace("\n", "")
            charList.append(line)
        file.close()
        return charList